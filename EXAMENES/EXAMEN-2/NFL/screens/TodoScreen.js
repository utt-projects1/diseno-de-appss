import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View, Animated, Easing } from 'react-native';

const TodoListItem = ({ id, title }) => {
  return (
    <Text style={styles.item}>{id} - {title}</Text>
  );
};

const TodoListScreen = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    fetchTodos();
  }, []);

  const fetchTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error fetching todos:', error);
    }
  };

  const getPendingTodos = () => {
    return todos.filter(todo => !todo.completed);
  };

  const getCompletedTodos = () => {
    return todos.filter(todo => todo.completed);
  };

  const rotateValueHolder = new Animated.Value(0);

  const startRotate = () => {
    rotateValueHolder.setValue(0);
    Animated.timing(
      rotateValueHolder,
      {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start(() => startRotate());
  };

  useEffect(() => {
    startRotate();
  }, []);

  const rotateData = rotateValueHolder.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  });

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Animated.Text style={[styles.heading, { transform: [{ rotate: rotateData }] }]}>Lista de Pendientes</Animated.Text>
      <Text>ID de todos los pendientes: {todos.map(todo => todo.id).join(', ')}</Text>
      <Text style={styles.subHeading}>ID y Título de todos los pendientes:</Text>
      <ScrollView style={styles.listContainer}>
        {todos.map(todo => (
          <TodoListItem key={todo.id} id={todo.id} title={todo.title} />
        ))}
      </ScrollView>
      <Text style={styles.subHeading}>ID y Título de todos los pendientes sin resolver:</Text>
      <ScrollView style={styles.listContainer}>
        {getPendingTodos().map(todo => (
          <TodoListItem key={todo.id} id={todo.id} title={todo.title} />
        ))}
      </ScrollView>
      <Text style={styles.subHeading}>ID y Título de todos los pendientes resueltos:</Text>
      <ScrollView style={styles.listContainer}>
        {getCompletedTodos().map(todo => (
          <TodoListItem key={todo.id} id={todo.id} title={todo.title} />
        ))}
      </ScrollView>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    padding: 20,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  subHeading: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
  },
  listContainer: {
    maxHeight: 200,
    marginBottom: 20,
  },
  item: {
    fontSize: 16,
    marginBottom: 5,
  },
});

export default TodoListScreen;
