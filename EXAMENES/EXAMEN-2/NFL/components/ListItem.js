import React from 'react';
import { StyleSheet, Text } from 'react-native';

const TodoListItem = ({ id, title }) => {
  return (
    <Text style={styles.item}>{id} - {title}</Text>
  );
};

const styles = StyleSheet.create({
  item: {
    fontSize: 16,
    marginBottom: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#fff',
    borderRadius: 5,
    elevation: 2,
  },
});

export default TodoListItem;
