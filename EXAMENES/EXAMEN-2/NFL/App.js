import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import TodoListScreen from './screens/TodoScreen';

const App = () => {
  return (
    <View style={styles.container}>
      <Image source={require('./assets/nfl.jpg')} style={styles.logo} />
      <Text style={styles.title}>NFL</Text>
      <TodoListScreen />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000', // Fondo negro
    paddingTop: 40,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center', // Alinear elementos verticalmente en el centro
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: '#fff', // Letras blancas
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
});

export default App;
