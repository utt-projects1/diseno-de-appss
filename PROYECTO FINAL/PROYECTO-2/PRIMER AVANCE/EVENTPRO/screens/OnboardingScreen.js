import React from "react";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import Onboarding from 'react-native-onboarding-swiper';
import LottieView from 'lottie-react-native';
import { useNavigation } from "@react-navigation/native";

const { width, height } = Dimensions.get('window');

export default function OnboardingScreen() {
    const navigation = useNavigation();

    const handleDone = () => {
        navigation.navigate('Home');
    }

    const doneButton = ({...props}) => {
        return (
            <TouchableOpacity style={styles.doneButton} {...props}>
                <Text>Done</Text>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.container}>
            <Onboarding
                onDone={handleDone}
                onSkip={handleDone}
                DoneButtonComponent={doneButton}
                containerStyles={{ paddingHorizontal: 15 }}
                pages={[
                    {
                        backgroundColor: '#C8E6C9',
                        image: (
                            <View style={styles.lottie}>
                                <LottieView source={require('../assets/animations/CONFETTI.json')} autoPlay loop />
                            </View>
                        ),
                        title: 'Boost Productivity',
                        subtitle: 'Welcome to our app! Boost your productivity with us.',
                    },
                    {
                        backgroundColor: '#FFECB3',
                        image: (
                            <View style={styles.lottie}>
                                <LottieView source={require('../assets/animations/CONFETTI.json')} autoPlay loop />
                            </View>
                        ),
                        title: 'Onboarding',
                        subtitle: 'Experience seamless onboarding with React Native Onboarding Swiper.',
                    },
                    {
                        backgroundColor: '#D1C4E9',
                        image: (
                            <View style={styles.lottie}>
                                <LottieView source={require('../assets/animations/CONFETTI.json')} autoPlay loop />
                            </View>
                        ),
                        title: 'Start Exploring',
                        subtitle: 'Start exploring the features of our app now!',
                    },
                ]}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    lottie: {
        width: width * 0.9,
        height: width * 0.9
    },
    doneButton: {
        padding: 20,
    },
});
