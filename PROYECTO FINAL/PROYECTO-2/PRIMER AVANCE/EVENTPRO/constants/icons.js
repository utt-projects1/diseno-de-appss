const Main = require('../assets/images/Main.png');
const Search = require('../assets/images/Search.png');
const Favorite = require('../assets/images/Favorite.png');
const User = require ('../assets/images/User.png');
const FavoriteFilled = require('../assets/images/Favorite_filled.png');
const Hamburger = require('../assets/images/Hamburger.png');
const Notification = require('../assets/images/Notification.png');
const ArrowLeft = require ('../assets/images/Arrow_Left.png')

export default {
  Main,
  FavoriteFilled,
  Favorite,
  User,
  Hamburger,
  Search,
  Notification,
  ArrowLeft,
};