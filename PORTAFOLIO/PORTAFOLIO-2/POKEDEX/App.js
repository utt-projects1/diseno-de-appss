import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import Pokedex from './components/Pokedex';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <StatusBar style="auto" />
      </View>
      <View style={styles.content}>
        <Pokedex />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f9fa',
  },
  header: {
    paddingTop: 40,
    backgroundColor: '#007bff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
});
