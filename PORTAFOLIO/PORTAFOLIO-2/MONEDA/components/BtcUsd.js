import React, { useState, useEffect } from 'react';
import { Text, View, ActivityIndicator, StyleSheet } from 'react-native';

const BtcUsd = () => {
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const url = "https://api.coindesk.com/v1/bpi/currentprice.json";

  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then((result) => {
        setIsLoading(false);
        setResponse(result);
      })
      .catch((error) => {
        setIsLoading(false);
        setError(error);
      });
  }, [url]);

  const getContent = () => {
    if (isLoading) {
      return (
        <View style={styles.container}>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      return (
        <View style={styles.container}>
          <Text>Error: {error.message}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={styles.textSize}>BTC to USD: ${response && response.bpi && response.bpi.USD.rate}</Text>
      </View>
    );
  };

  return getContent();
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    alignItems: 'center',
  },
  textSize: {
    fontSize: 20
  },
});

export default BtcUsd;
